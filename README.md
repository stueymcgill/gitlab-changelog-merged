In this project the following steps were taken:

- Add tag 1.0.0 on main
- Create `my-branch`
- Add a commit with a git trailer
- Merge `my-branch`

The Gitlab Changelog GET API (https://gitlab.com/api/v4/projects/45243954/repository/changelog?version=1.0.1)
does pick up the change.